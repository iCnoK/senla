package FifthTask;

import java.util.Arrays;
import java.util.Scanner;

public class FifthTask {

    public static void main(String[] args) {
        int min = 0;
        int max = 100;
        int N = GetNumberFromConsole("Enter N: ", min, max);
        var palindromes = Palindrome.getPalindromes(min, N);
        System.out.print("Number of palindromes = " + palindromes.size() + "\n" +
                Arrays.toString(palindromes.toArray()));
    }

    private static int GetNumberFromConsole(String message, int minInteger, int maxInteger) {
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        String input = sc.next();
        int result = 0;
        try {
            result = Integer.parseInt(input);
            if (result < minInteger || result > maxInteger) {
                throw new NumberFormatException();
            }
            return result;
        } catch (NumberFormatException e) {
            System.out.print("Incorrect input!\n");
            return GetNumberFromConsole(message, minInteger, maxInteger);
        }
    }
}
