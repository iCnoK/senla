package FifthTask;

import java.util.ArrayList;
import java.util.List;

public class Palindrome {

    public static List<Integer> getPalindromes(int begin, int end) {
        List<Integer> palindromes = new ArrayList<Integer>();
        for (int i = begin; i < end; i++) {
            if (isPalindrome(i)) {
                palindromes.add(i);
            }
        }
        return palindromes;
    }

    private static boolean isPalindrome(int number) {
        int temp = number;
        int mirrorNumber = 0;
        while (temp != 0) {
            mirrorNumber = (mirrorNumber * 10) + (temp % 10);
            temp /= 10;
        }
        if (number == mirrorNumber) {
            return true;
        } else {
            return false;
        }
    }
}
