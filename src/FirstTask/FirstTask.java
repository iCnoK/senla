package FirstTask;

import java.util.Scanner;

public class FirstTask {

    public static void main(String[] args) {
        int input = GetNumberFromConsole("Enter integer number > 0: ");
        var number = new IntegerNumber(input);
        System.out.print("Is even: " + number.getIsEven() +
                "\nIs odd: " + number.getIsOdd() +
                "\nIs prime: " + number.getIsPrime() +
                "\nIs composite: " + number.getIsComposite());
    }

    private static int GetNumberFromConsole(String message) {
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        String input = sc.next();
        int result = 0;
        try {
            result = Integer.parseInt(input);
            if (result == 0) {
                throw new NumberFormatException();
            }
            return result;
        } catch (NumberFormatException e) {
            System.out.print("Incorrect input!\n");
            return GetNumberFromConsole(message);
        }
    }
}
