package FirstTask;

import static java.lang.Math.sqrt;

public class IntegerNumber {

    private boolean isEven;

    private boolean isOdd;

    private boolean isPrime;

    private boolean isComposite;

    public IntegerNumber(int number) {
        isEven = IsNumberEven(number);
        isOdd = !isEven;
        isComposite = IsComposite(number);
        isPrime = IsPrime(number);
    }

    public boolean getIsEven() {
        return isEven;
    }

    public boolean getIsOdd() {
        return isOdd;
    }

    public boolean getIsPrime() {
        return isPrime;
    }

    public boolean getIsComposite() {
        return isComposite;
    }

    private boolean IsNumberEven(int number) {
        return number % 2 == 0;
    }

    public boolean IsPrime(int number) {
        if (number == 1) {
            return false;
        }
        for (int i = 2; i < sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public boolean IsComposite(int number) {
        if (number == 1) {
            return false;
        }
        return !IsPrime(number);
    }
}
