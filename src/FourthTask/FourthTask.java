package FourthTask;

import java.util.Scanner;

public class FourthTask {
    public static void main(String[] args) {
        var sentence = getStringFromConsole("Enter sentence: ");
        var word = getStringFromConsole("Enter word: ");

        System.out.print("Number of coincidences = " + Sentence.findAllMatchesCount(sentence, word));
    }

    public static String getStringFromConsole(String message) {
        System.out.print(message);
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }
}
