package FourthTask;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
    public static int findAllMatchesCount(String sentence, String word) {
        var loweredSentence = sentence.toLowerCase();
        var loweredWord = word.toLowerCase();
        Pattern pattern = Pattern.compile(loweredWord);
        Matcher matcher = pattern.matcher(loweredSentence);
        int counter = 0;
        while (matcher.find()) {
            counter++;
        }
        return counter;
    }
}
