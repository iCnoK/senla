package SecondTask;

public interface NumberService {
    public int getNOD(int number1, int number2);

    public int getNOK(int number1, int number2);
}
