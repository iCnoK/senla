package SecondTask;

public class NumberServiceImpl implements NumberService {
    @Override
    public int getNOD(int number1, int number2) {
        if (number1 != 0) {
            return getNOD(number2 % number1, number1);
        } else {
            return number2;
        }
    }

    @Override
    public int getNOK(int number1, int number2) {
        return (number1 / getNOD(number1, number2)) * number2;
    }
}
