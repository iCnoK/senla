package SecondTask;

import java.util.Scanner;

public class SecondTask {
    public static void main(String[] args) {
        var numberService = new NumberServiceImpl();
        System.out.print("Enter 2 integer numbers:\n");
        int number1 = GetNumberFromConsole("1: ");
        int number2 = GetNumberFromConsole("2: ");
        System.out.print("Your numbers: " + number1 + " : " + number2 +
                "\nNOD = " + numberService.getNOD(number1, number2) +
                "\nNOK = " + numberService.getNOK(number1, number2));
    }

    private static int GetNumberFromConsole(String message) {
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        String input = sc.next();
        int result = 0;
        try {
            result = Integer.parseInt(input);
            if (result == 0) {
                throw new NumberFormatException();
            }
            return result;
        } catch (NumberFormatException e) {
            System.out.print("Incorrect input!\n");
            return GetNumberFromConsole(message);
        }
    }
}
