package SixthTask;

import java.util.ArrayList;

public class Backpack {

    private int capacity;

    private ArrayList<Thing> things;

    public ArrayList<Thing> getThings() {
        return things;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getThingsFullness() {
        if (things.size() == 0) return 0;
        int counter = 0;
        for (int i = 0; i < things.size(); i++) {
            counter += things.get(i).getWeight();
        }
        return counter;
    }

    public int getTotalCostOfThings() {
        if (things.size() == 0) return 0;
        int counter = 0;
        for (int i = 0; i < things.size(); i++) {
            counter += things.get(i).getCost();
        }
        return counter;
    }

    public Backpack(int backpackCapacity) {
        this.capacity = backpackCapacity;
    }

    public void fillBackpack(ArrayList<Thing> things) {
        var weightsArray = getWeightsArray(things);
        var pricesArray = getPricesArray(things);
        var mainPriceArray = getPriceArray(getCapacity(), things.size(), weightsArray, pricesArray);

        var resultThingsIndexes = new ArrayList<Integer>();
        findThingsIndexes(things.size() - 1, getCapacity() - 1, mainPriceArray, weightsArray, resultThingsIndexes);

        this.things = fillBackpackWithThings(resultThingsIndexes, things);
    }

    private int[] getWeightsArray(ArrayList<Thing> things) {
        int[] array = new int[things.size()];
        for (int i = 0; i < things.size(); i++) {
            array[i] = things.get(i).getWeight();
        }
        return array;
    }

    private int[] getPricesArray(ArrayList<Thing> things) {
        int[] array = new int[things.size()];
        for (int i = 0; i < things.size(); i++) {
            array[i] = things.get(i).getCost();
        }
        return array;
    }

    private int[][] getPriceArray(int backpackCapacity, int thingsNumber, int[] weightsArray, int[] pricesArray) {
        int[][] array = new int[thingsNumber][backpackCapacity];

        for (int i = 0; i < backpackCapacity; i++) {
            array[0][i] = 0;
        }
        for (int k = 1; k < thingsNumber; k++) {
            for (int s = 0; s < backpackCapacity; s++) {
                if (weightsArray[k] > s) {
                    array[k][s] = array[k - 1][s];
                } else {
                    array[k][s] = Math.max(array[k - 1][s], array[k - 1][s - weightsArray[k]] + pricesArray[k]);
                }
            }
        }
        return array;
    }

    private void findThingsIndexes(int k, int s, int[][] mainPriceArray, int[] weightsArray, ArrayList<Integer> result) {
        if (mainPriceArray[k][s] == 0) return;
        if (mainPriceArray[k - 1][s] == mainPriceArray[k][s]) {
            findThingsIndexes(k - 1, s, mainPriceArray, weightsArray, result);
        } else {
            findThingsIndexes(k - 1, s - weightsArray[k], mainPriceArray, weightsArray, result);
            result.add(k);
        }
    }

    private ArrayList<Thing> fillBackpackWithThings(ArrayList<Integer> thingsIndexes, ArrayList<Thing> allThings) {
        var result = new ArrayList<Thing>();
        for (int i = 0; i < thingsIndexes.size(); i++) {
            var thing = allThings.get(thingsIndexes.get(i));
            result.add(thing);
        }
        return result;
    }
}
