package SixthTask;

import java.util.*;

/*В данной задаче реализован алгоритм, в который подается максимальный объем рюкзака и максимальное количество вещей в нем*/

public class SixthTask {
    public static void main(String[] args) {

        int backpackCapacity = getNumberFromConsole("Enter backpack capacity: ", 1);
        int numberOfThings = getNumberFromConsole("Enter number of things: ", 1);
        var things = new ArrayList<Thing>();

        for (int i = 0; i < numberOfThings; i++) {
            var thing = getThingFromConsole(i + 1);
            things.add(thing);
        }

        var backpack = new Backpack(backpackCapacity);
        backpack.fillBackpack(things);

        var thingsInBackpack = backpack.getThings();
        var thingsFullness = backpack.getThingsFullness();
        var totalThingsCost = backpack.getTotalCostOfThings();
        showThings(thingsInBackpack, thingsFullness, totalThingsCost);
    }

    private static void showThings(ArrayList<Thing> things, int thingsFullness, int thingsCost) {
        System.out.print("Occupied space in a backpack: " + thingsFullness +
                "\nTotal things cost: " + thingsCost +
                "\nThings in backpack:");
        for (int i = 0; i < things.size(); i++) {
            System.out.print("\nThing #" + i +
                    "\nWeight: " + things.get(i).getWeight() +
                    "\nCost: " + things.get(i).getCost());
        }
    }

    private static Thing getThingFromConsole(int index) {
        System.out.print("Enter thing #" + index + "\n");
        int weight = getNumberFromConsole("Enter weight of thing #" + index + " ", 0);
        int cost = getNumberFromConsole("Enter cost of thing #" + index + " ", 0);
        return new Thing(weight, cost);
    }

    private static int getNumberFromConsole(String message, int minInteger) {
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        String input = sc.next();
        int result = 0;
        try {
            result = Integer.parseInt(input);
            if (result < minInteger) {
                throw new NumberFormatException();
            }
            return result;
        } catch (NumberFormatException e) {
            System.out.print("Incorrect input!\n");
            return getNumberFromConsole(message, minInteger);
        }
    }
}
