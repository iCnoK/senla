package SixthTask;

public class Thing {

    private int weight;
    private int cost;

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        if (weight > 0) {
            this.weight = weight;
        }
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        if (cost > 0) {
            this.cost = cost;
        }
    }

    public Thing(int weight, int cost) {
        setWeight(weight);
        setCost(cost);
    }

    @Override
    public String toString() {
        return "|Weight = " + weight + ", Cost = " + cost + "|";
    }
}
