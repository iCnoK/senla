package ThirdTask;

import java.util.Arrays;

public class Sentence {

    private String[] words;

    public Sentence(String sentence) {
        words = parseSentence(sentence);
    }

    @Override
    public String toString() {
        var string = Arrays.toString(words);
        string = string.substring(1, string.length() - 1);
        return string;
    }

    public String toString(String[] words) {
        var string = Arrays.toString(words);
        string = string.substring(1, string.length() - 1);
        return string;
    }

    public int getWordsCount() {
        return words.length;
    }

    public String[] getWords() {
        return words.clone();
    }

    public String[] sort() {
        var words = getWords();
        Arrays.sort(words);
        return words;
    }

    public String[] sort(String[] words) {
        var newWords = words.clone();
        Arrays.sort(newWords);
        return newWords;
    }

    public String[] firstLetterToUpper() {
        var words = getWords();
        for (int i = 0; i < this.words.length; i++) {
            words[i] = firstLetterToUpper(words[i]);
        }
        return words;
    }

    public String[] firstLetterToUpper(String[] words) {
        var newWords = words.clone();
        for (int i = 0; i < words.length; i++) {
            newWords[i] = firstLetterToUpper(newWords[i]);
        }
        return newWords;
    }

    private String[] parseSentence(String sentence) {
        return sentence.split(" ");
    }

    private String firstLetterToUpper(String word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
}
