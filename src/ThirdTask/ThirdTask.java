package ThirdTask;

import java.util.Scanner;

public class ThirdTask {
    public static void main(String[] args) {
        var sentenceStr = getStringFromConsole("Write your sentence: ");
        var sentence = new Sentence(sentenceStr);

        var sortedSentence = sentence.sort();
        var firstLetterToUpperSentence = sentence.firstLetterToUpper();
        var sortedFirstLetterToUpperSentence = sentence.firstLetterToUpper(sortedSentence);

        System.out.print("Words count: " + sentence.getWordsCount() +
                "\nSorted: " + sentence.toString(sortedSentence) +
                "\nFirst letter to upper: " + sentence.toString(firstLetterToUpperSentence) +
                "\nSorted first letter to upper: " + sentence.toString(sortedFirstLetterToUpperSentence));
    }

    public static String getStringFromConsole(String message) {
        System.out.print(message);
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }
}
